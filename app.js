const express = require('express')
const path = require('path')

let port = process.env.PORT;
if (port == null || port == '') {
  port = 8000;
}


express()
.use(express.static(path.join(__dirname)))
.get('/', (req, res) => {
    res.sendFile('index.html', {
        root: path.join(__dirname, './')
    })
})
.listen(port, () => console.log(`Listening on port ${port}`))